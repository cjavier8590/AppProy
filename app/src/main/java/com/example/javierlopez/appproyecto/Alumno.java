package com.example.javierlopez.appproyecto;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Alumno extends AppCompatActivity implements Serializable  {

    private EditText nombre, apellido, telefono, noCarne, fechaNac;
    private Switch genero;

    private List<AlumnoClase> listado = new ArrayList<>();
    private ArrayAdapter<AlumnoClase> adapter;
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno);

        nombre = (EditText) findViewById(R.id.nombre);
        apellido = (EditText) findViewById(R.id.apellido);
        telefono = (EditText) findViewById(R.id.telefono);
        noCarne = (EditText) findViewById(R.id.carne);
        fechaNac = (EditText) findViewById(R.id.fechaNac);
        genero = (Switch) findViewById(R.id.genero);

        lv= (ListView) findViewById(R.id.lv );



    }

    public void guarda(View view){

        AlumnoClase alumno = new AlumnoClase();
        alumno.setNombre(nombre.getText().toString());
        alumno.setApellido(apellido.getText().toString());
        alumno.setTelefono(telefono.getText().toString());
        alumno.setNoCarne(noCarne.getText().toString());
        //alumno.setFechaNac(Date.valueOf(fechaNac.getText().toString()));
        //alumno.setGenero(Boolean.parseBoolean(genero.getText().toString()));
        listado.add(alumno);
        Toast notification = Toast.makeText(this, "Alumno almacenado exitosamente", Toast.LENGTH_SHORT);
        notification.show();

        adapter=new ArrayAdapter<AlumnoClase>(this,android.R.layout.simple_list_item_1, listado);
        lv.setAdapter(adapter);


    }

    public void listado(View view){

        Intent i = new Intent(this, ListaAlumno.class);
        i.putExtra("LIST", (Serializable) listado);
        startActivity(i);
    }
}
