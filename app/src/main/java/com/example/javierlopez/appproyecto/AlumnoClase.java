package com.example.javierlopez.appproyecto;

import java.util.Date;

/**
 * Created by Javier Lopez on 27/07/2017.
 */

public class AlumnoClase {

    String nombre;
    String apellido;
    Date fechaNac;
    boolean genero;
    String telefono;
    String noCarne;


    public AlumnoClase(String nombre, String apellido, Date fechaNac, boolean genero, String telefono, String noCarne) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNac = fechaNac;
        this.genero = genero;
        this.telefono = telefono;
        this.noCarne = noCarne;
    }

    public AlumnoClase() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public boolean isGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNoCarne() {
        return noCarne;
    }

    public void setNoCarne(String noCarne) {
        this.noCarne = noCarne;
    }
}
